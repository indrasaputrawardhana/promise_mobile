<?php

class Login_form extends CI_Controller {

    public function index() {

        $data['title'] = 'Form Login';
        $data['view_file'] = 'login';
        $data['view_folder'] = 'login_form';
        $data['token'] = $this->security->get_csrf_hash();

        $this->load->view('layout/login', $data);
    }

    public function username_pass_check() {
        $username = $this->input->post('username', TRUE);
        $pwd = $this->input->post('pwd', TRUE);

        /*
         * 
         */
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => URL_SOA_GEDUNG,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n\t\"username\": \"$username\",\n\t\"password\": \"$pwd\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
            ),
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
 
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response, $assoc = TRUE);
            //if authenticated
            if ($response['metaData']['code'] == '200') {
                $this->session->set_userdata('username', $username);
                $this->session->set_userdata('token_login', $response['response']['tokenLogin']);
                $this->session->set_userdata('namavendor', $response['response']['namavendor']);
                $this->session->set_userdata('kodevendor', $response['response']['kodevendor']);
                $this->session->set_userdata('email', $response['response']['email']);
                $this->session->set_userdata('alamat', $response['response']['alamat']);
                $this->session->set_userdata('logged_in', TRUE);
                redirect('project_context_controller');
            }
            //else
            if ($response['metaData']['code'] == '400' || $response['metaData']['code'] == '401') {
                $data['title'] = 'Form Login';
                $data['view_file'] = 'login';
                $data['view_folder'] = 'login_form';
                $data['token'] = $this->security->get_csrf_hash();
                $data['error_message'] = $response['metaData']['message'];
                $this->load->view('layout/login', $data);
            }
        }
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('login_form/index');
    }

}
