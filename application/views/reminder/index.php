<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<div class="container">
    <!-- Page Heading -->
    <div class="row">
        <h1 class="page-header">Reminder task
            <div class="pull-right"><a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#ModalaAdd"><span class="glyphicon glyphicon-plus-sign"></span></a></div>
        </h1> 
    </div>
    <div class="row">
        <div id="reload">
            <table class="table table-striped" id="mydata">
                <thead>
                    <tr>
                        <th>Task</th>
                        <th>Tanggal</th>
                        <th>Due</th>
                        <th style="text-align: right;">Action </th>
                    </tr>
                </thead>
                <tbody id="show_data">
                </tbody> 
            </table>
        </div>
    </div>
</div>
 
<!-- MODAL ADD -->
<div class="modal fade" id="ModalaAdd" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Tambah Reminder</h3>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">
                     
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Task</label>
                        <div class="col-xs-8" >
                            <input name="task" id="task_id" class="form-control" type="text" placeholder="Task" required>
                        </div>
                    </div>
                     
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Tanggal</label>
                        <div class="col-xs-9">
                            <p><input type="text" id="tanggal_id" style="visibility: hidden"></p>
                            <div id="tanggal_date_picker"></div>

                        </div>
                    </div>
                </div>
                 
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info" id="btn_simpan">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--END MODAL ADD-->
 
<!--MODAL HAPUS-->
<div class="modal fade" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title" id="myModalLabel">Hapus reminder</h4>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">
                     
                    <input type="hidden" name="kode" id="textkode" value="">
                    <div class="alert alert-warning"><p>Apakah Anda yakin melanjutkan?</p></div>
                     
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button class="btn_hapus btn btn-danger" id="btn_hapus">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--END MODAL HAPUS-->
  
<script lang="JavaScript">
    $(document).ready(function () {

        $("#tanggal_date_picker").datepicker({
            onSelect: function (dateText, inst) {
                var dateAsString = dateText; //the first parameter of this function
                document.getElementById("tanggal_id").value = formatDate(dateAsString);
            }
        });
        function formatDate(date) {
            var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [year, month, day].join('-');
        }

        tampil_data_reminder(); //pemanggilan fungsi tampil barang.
        //fungsi tampil schedule
        function tampil_data_reminder() {
            var url = "<?php echo base_url(); ?>reminder_controller/show_reminder";

            $.ajax({
                type  : 'GET',
                url: url,
                async: true,
                dataType: 'json',
                "crossDomain": true,

                success: function (data) {
                    var today = new Date();
                    var today_date = today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate();
                    console.log(data);

                    var html = '';
                    var i;
                    for (i = 0; i < data.length; i++) {
                        html += '<tr>' +
                                '<td>' + data[i].task + '</td>' +
                                '<td>' + data[i].tanggal + '</td>' +
                                '<td>' + data[i].due + ' hari </td>' +
                                '<td style="text-align:right;">' +
                                '<a href="javascript:;" class="btn btn-danger btn-xs item_hapus" data="' + data[i].id_task + '"><span class="glyphicon glyphicon-remove"></span></a>' +
                                '</td>' +
                                '</tr>';
                    }
                    $('#show_data').html(html);
                }
 
            });
        }
 
 
        //KONFIRMASI HAPUS
        $('#show_data').on('click', '.item_hapus', function () {
            var id = $(this).attr('data');
            $('#ModalHapus').modal('show');
            $('[name="kode"]').val(id);
        });
           //Hapus Barang
        $('#btn_hapus').on('click', function () {
            var kode = $('#textkode').val();
            var url = "<?php echo base_url(); ?>reminder_controller/hapus_task";
            var token = '<?php echo $token; ?>';

            $.ajax({
                type: "POST",
                url  : url,
                dataType: "JSON",
                data: {id_task: kode, csrf_token: token},
                error: function (data) {
                    $('#ModalHapus').modal('hide');
                    tampil_data_reminder();
                }
            });
            return false;
        });

        //Simpan Barang
        $('#btn_simpan').on('click', function () {
            var task = $('#task_id').val();
            var tanggal = $('#tanggal_id').val();
            var url = "<?php echo base_url(); ?>reminder_controller/simpan_task";
            var token = '<?php echo $token; ?>';
            $.ajax({
                type: "POST",
                url  : url,
                dataType: "JSON",
                data: {task: task, tanggal: tanggal, csrf_token: token},
                error: function (data) {
                    $('[name="task"]').val("");
                    $('#tanggal_id').val("");
                    $('#ModalaAdd').modal('hide');
                    tampil_data_reminder();
                },
            });
            return false;
        });
 
    });
 
</script>

<!--hidden object-->
<div style="visibility: hidden" id="tanggal_variable"></div>


<script lang="JavaScript">
    $("#reminder_task_link").addClass('active');
</script>