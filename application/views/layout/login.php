<html>
    <head>
        <title><?php
            if (isset($title)) {
                echo $title;
            } else
                echo 'no title';
            ?>
        </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    </head>
    <body>
        <div style="padding: 30px; width: 100%; ">
            <img class="img-responsive" style="margin: auto" src="https://seeklogo.com/images/B/bpjs-kesehatan-logo-B436CE3991-seeklogo.com.png" alt="BPJS Logo">
        </div>
        <?php
        $this->load->view('./' . $view_folder . '/' . $view_file);
        ?>
    </body>
</html>