<script src="<?php echo base_url(); ?>assets/javascripts/date_timer.js"></script>
<script src="<?php echo base_url(); ?>assets/javascripts/users_geolocation.js"></script>

<div class="alert alert-info">
    Proyek : <strong><?php echo $this->session->userdata('nama_proyek'); ?> (<?php echo $this->session->userdata('lat') . "  " . $this->session->userdata('long'); ?>)</strong> <br>
    Longitude anda : <div id='longitude_position'></div>
    Latitude anda : <div id='latitude_position'></div><br>
    <div id='jam' ></div>
    <script language='JavaScript'>document.write(tanggallengkap);</script>

</div>
<div id="any_info_id" style="margin: auto;text-align: center">   </div>

<div class="container">

    <table class="table table-condensed">
        <thead>
            <tr>
                <th>Presensi</th>
                <th>Waktu</th>
            </tr>
        </thead>
        <tbody id="show_data_presensi">
            <!--show data presensi here-->
        </tbody>

    </table>

    <!--form image here-->
    <!--existing eviden image-->
    <div style="text-align: center" id="image_eviden_existing">
    </div>

    <!--preview image-->
    <div style="text-align: center" id="image_preview">
        <img style="margin: auto;width: 100%" id="previewing" src="" />
    </div>

    <hr id="line">
    <div id="selectImage">
        <label>Take image</label><br/>
        <input style="width: 100%" class="btn btn-warning"  type="file" name="file" id="file" accept="image/*" multiple="multiple" required />
        <input type="text" name="csrf_token" value="<?php echo $token; ?>" id="csrf_token" style="visibility: hidden"/>
        <div id="any_info_id_bawah" style="margin: auto;text-align: center">   </div>
        <!--<input style="width: 100%" class="btn btn-primary"  type="submit" value="Upload" class="submit" />-->
        <textarea id="foto" name="foto"  style="visibility: hidden"> </textarea>
    </div>
    <!--form image ends here-->

</div>

<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUzz1zAEMeDzquaroYBj2Lvy6II8Sh5Q8&callback=myMap"></script>-->
<script lang="JavaScript">


    $(document).ready(function (e) {
        var html = '  <strong> mohon ditunggu mendeteksi posisi ...</strong> <img style="height: 100px;margin: auto;" class="img-responsive" src="<?php echo base_url(); ?>assets/images/loader.gif" alt="Chania"> ';
        document.getElementById("any_info_id").innerHTML = html;

        navigator.geolocation.getCurrentPosition(success, error, options);

        tampil_data_presensi();
        tampil_data_image_presensi();


        // Function to preview image after validation
        $(function () {
            $("#file").change(function () {
                $("#message").empty(); // To remove the previous error message
                var file = this.files[0];
                var imagefile = file.type;
                var match = ["image/jpeg", "image/png", "image/jpg"];
                if ((Date.now() - file.lastModified) >= (60 * 1000)) {
                    bootbox.alert("foto ini diambil selain dari kamera , mohon gunakan kamera saja");
                } else {
                    //bila file dibuat less than 1 menit , lanjutkan upload file
                    if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
                    {
                        $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                        return false;
                    } else
                    {
                        var reader = new FileReader();
                        reader.onload = imageIsLoaded;
                        loadImageFile();
                        reader.readAsDataURL(this.files[0]);
                    }
                }
            });
        });

        function imageIsLoaded(e) {
            $("#file").css("color", "green");
            $('#image_preview').css("display", "block");
            $('#previewing').attr('src', e.target.result);
            $('#image_preview').css("width", "85%");
            $('#image_preview').css("margin", "auto");
            $('#previewing').addClass("img-responsive");
        }
        ;

    });
    //functions go here

    // beginning resizing image
    //beginning uploading photos
    function upload_image() {
        var url = "<?php echo base_url() ?>presensi_controller/upload_image";
        var token = '<?php echo $token; ?>';
        var data = new FormData();
        data.append('foto', document.getElementById('foto').value);
        data.append('csrf_token', '<?php echo $token; ?>');
        //kode presensi 0 masuk , 1 pulang
        data.append('kode_presensi', '1');

        $.ajax({
            url: url, // Url to which the request is send
            type: "POST", // Type of request to be send, called as method
            data: data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false, // To send DOMDocument or non processed data file it is set to false
            beforeSend: function () {

                var html = '  <strong> sedang mengupload images mohon tidak reload dan tidak menekan apa-apa ...</strong> <img style="height: 100px;margin: auto;" class="img-responsive" src="<?php echo base_url(); ?>assets/images/loader.gif" alt="Chania"> ';
                document.getElementById("any_info_id_bawah").innerHTML = html;
            },
            success: function (data)   // A function to be called if request succeeds
            {
                var html = '';
                document.getElementById("any_info_id_bawah").innerHTML = html;
                console.log('sukses upload image');
                tampil_data_image_presensi();

            },
            error: function (data) {
                console.log('eror upload image');
            }
        });
    }
    ;
    //ending uploading photos

    function hapus_data_image_presensi(id, image_name) {
        bootbox.confirm({
            message: "yakin hendak menghapus image tersebut?",
            buttons: {
                confirm: {
                    label: 'Ya',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Cancel',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result === true) {

                    var url = "<?php echo base_url(); ?>presensi_controller/delete_image_presensi";
                    var token = '<?php echo $token; ?>';

                    $.ajax({
                        type  : 'POST',
                        url: url,
                        async: true,
                        dataType: 'json',
                        "crossDomain": true,
                        data: {id: id, filename: image_name, csrf_token: token},
                        success: function (data) {
                            var html = '';
                            var i;
                            for (i = 0; i < data.length; i++) {
                                html += '<img style="margin: auto;width: 100%"  src="<?php echo base_url(); ?>assets/images/' + data[i].file_name + '" />'
                            }
                            $('#image_eviden_existing').html(html);
                            tampil_data_image_presensi();
                            var html = '';
                            document.getElementById("any_info_id").innerHTML = html;
                        },
                    });
                }
            }
        });


    }

    var fileReader = new FileReader();
    var filterType = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

    fileReader.onload = function (event) {
        var image = new Image();
        image.onload = function () {
            var canvas = document.createElement("canvas");
            var context = canvas.getContext("2d");
            var dimension = resize_image_to_desktop_size(image.width, image.height);
            canvas.width = dimension[0];
            canvas.height = dimension[1];

            context.drawImage(image,
                    0,
                    0,
                    image.width,
                    image.height,
                    0,
                    0,
                    canvas.width,
                    canvas.height
                    );

            document.getElementById("foto").value = canvas.toDataURL();
            upload_image();
        }
        image.src = event.target.result;
    };
    function resize_image_to_desktop_size(w, h) {

        //target width=200
        //target heigh=150
        var desired_height = 150;
        var desired_width = 200;
        var rasio = (w / desired_width);
        var destination_height = (h / rasio);
        var array = [desired_width, destination_height];
        return array;
    }

    var loadImageFile = function () {
        var uploadImage = document.getElementById("file");

        //check and retuns the length of uploded file.
        if (uploadImage.files.length === 0) {
            return;
        }

        //Is Used for validate a valid file.
        var uploadFile = document.getElementById("file").files[0];
        if (!filterType.test(uploadFile.type)) {
            alert("Please select a valid image.");
            return;
        }
        fileReader.readAsDataURL(uploadFile);
    }
    // ending resizing image

    function tampil_data_image_presensi() {
        var html = '  <strong> menampilkan data image presensi ...</strong> <img style="height: 100px;margin: auto;" class="img-responsive" src="<?php echo base_url(); ?>assets/images/loader.gif" alt="Chania"> ';
        document.getElementById("any_info_id").innerHTML = html;

        var url = "<?php echo base_url(); ?>presensi_controller/tampilkan_eviden_presensi_pulang";
        var token = '<?php echo $token; ?>';

        $.ajax({
            type  : 'POST',
            url: url,
            async: true,
            dataType: 'json',
            "crossDomain": true,

            data: {csrf_token: token},
            success: function (data) {
                var html = '';
                var i;
                for (i = 0; i < data.length; i++) {
                    html += '<a href="#"><span onclick="hapus_data_image_presensi(' + data[i].id_eviden_foto + ',\'' + data[i].file_name + '\')" class="glyphicon glyphicon-remove-sign"></span></a>';
                    html += '<img style="margin: auto;width: 100%"  src="<?php echo base_url(); ?>assets/images/' + data[i].file_name + '" />'
                }
                $('#image_eviden_existing').html(html);
                $('#image_preview').html('<img style="margin: auto;width: 100%" id="previewing" src="" />');

                var html = '';
                document.getElementById("any_info_id").innerHTML = html;
            },
        });
    }
    function scan_pulang() {
        var html = '  <strong> scanning lokasi device ...</strong> <img style="height: 100px;margin: auto;" class="img-responsive" src="<?php echo base_url(); ?>assets/images/loader.gif" alt="Chania"> ';
        document.getElementById("any_info_id").innerHTML = html;
        cari_jarak();
    }
    function cari_jarak() {
        var html = '  <strong> menghitung jarak ke lokasi proyek ...</strong> <img style="height: 100px;margin: auto;" class="img-responsive" src="<?php echo base_url(); ?>assets/images/loader.gif" alt="Chania"> ';
        document.getElementById("any_info_id").innerHTML = html;

        var lat = document.getElementById("latitude_position").innerHTML;
        var long = document.getElementById("longitude_position").innerHTML;
        var url = "<?php echo base_url(); ?>presensi_controller/get_distance";
        var token = '<?php echo $token; ?>';
        $.ajax({
            type: "POST",
            url  : url,
            dataType: "JSON",
            data: {lat: lat, csrf_token: token, long: long},
            error: function (data) {
                var jarak = data.responseText;
                simpan_presensi_pulang(jarak);
            },
        });
    }

    function simpan_presensi_pulang(jarak) {
        var html = '  <strong> menyimpan data presensi ke server ...</strong> <img style="height: 100px;margin: auto;" class="img-responsive" src="<?php echo base_url(); ?>assets/images/loader.gif" alt="Chania"> ';
        document.getElementById("any_info_id").innerHTML = html;

        var lat = document.getElementById("latitude_position").innerHTML;
        var long = document.getElementById("longitude_position").innerHTML;
        var jarak = jarak;
        var url = "<?php echo base_url(); ?>presensi_controller/simpan_presensi_pulang";
        var token = '<?php echo $token; ?>';
        $.ajax({
            type: "POST",
            url  : url,
            dataType: "JSON",
            data: {lat: lat, long: long, csrf_token: token, jarak: jarak},
            error: function (data) {
                tampil_data_presensi();
            },
        });
        return false;
    }

    function tampil_data_presensi() {
        var html = '  <strong> menampilkan data presensi ...</strong> <img style="height: 100px;margin: auto;" class="img-responsive" src="<?php echo base_url(); ?>assets/images/loader.gif" alt="Chania"> ';
        document.getElementById("any_info_id").innerHTML = html;

        var url = "<?php echo base_url(); ?>presensi_controller/tampilkan_presensi_pulang";
        var token = '<?php echo $token; ?>';

        $.ajax({
            type  : 'POST',
            url: url,
            async: true,
            dataType: 'json',
            "crossDomain": true,

            data: {csrf_token: token},
            success: function (data) {
                console.log('tampil data  presensi finish..');

                var html = '';
                var i;
                for (i = 0; i < data.length; i++) {
                    html += '<tr><td>Presensi Waktu Pulang</td><td id="scan_datang">' + data[i].date_created + ' at ' + data[i].latitude + ' ' + data[i].longitude + ' ' + data[i].jarak_dari_project + '  dari lokasi project</td>     </tr>  ';
                }
                html += '<tr><td colspan="2"> <button style="width: 100%" type="button" class="btn btn-secondary" id="scan_pulang_id" onclick="scan_pulang()">Scan Pulang</button> </td>            </tr>';
                $('#show_data_presensi').html(html);

                var html = '';
                document.getElementById("any_info_id").innerHTML = html;
            },
        });
    }
</script>