

<div class="container">
    <h3 style="text-align: center">PROJECT MONITORING SISTEM (PROMISE)</h3>
    <?php if (isset($error_message)) { ?>
        <div class="alert alert-info alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <p><?php echo $error_message; ?></p>
        </div>
        <?php
    }
    ?>
    <form action="<?php echo base_url();?>login_form/username_pass_check" method="post">
        <div class="form-group">
            <label for="username">Username:</label>
            <input class="form-control"  placeholder="Enter username" name="username">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
            <input style="visibility: hidden" type="text" id="csrf_token" value="<?php echo $token;?>" name="csrf_token">
        </div>
        <button type="submit" class="btn btn-success" style="width: 100%">LOGIN</button><br>
        <button type="button" class="btn btn-primary" style="width: 100%" data-toggle="modal" data-target="#lupa_password_modal" data-whatever="@mdo">LUPA PASSWORD</button><br>
        <button type="button" class="btn btn-secondary" style="width: 100%" data-toggle="modal" data-target="#syarat_ketentuan">SYARAT DAN KETENTUAN</button><br>
    </form>
</div>


<!--begin of lupa password-->
<div class="modal fade" id="lupa_password_modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">LUPA PASSWORD</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Alamat Email:</label>
                        <input type="text" class="form-control" id="alamat_email">
                        <div id='result'></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="send">Send message</button>
            </div>
        </div>
    </div>
</div>
<!--endof lupa password-->

<!--begin of syarat ketentuan--> 
<div class="modal fade" id="syarat_ketentuan" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">SYARAT DAN KETENTUAN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="text-align: justify;width: 100%;">
                    This regex eliminates valid, in-use emails. Do not use. Google for "RFC822" or "RFC2822" to get a proper regex. – Randal Schwartz Sep 8 '10 at 2:34
                    @Randall: Can you give an example of an email address that this won't let through? – rossipedia Sep 14 '10 at 18:08
                    @GoodPerson I just tried to email n@ai to tell him/her they have a cool email address. But alas, gmail wouldn't let me. I suspect whoever that is has bigger problems communicating with others via email than just my site's javascript validation! But thanks for rising to the challenge. – Ben Roberts Jan 13 '13 at 7:38 
                    For all the people commenting that this is "good enough": look, you're simply thinking about this problem wrong. That's OK. It's a choice you can make for your users. I ain't mad at it. But, you know, technically speaking, you're demonstratively, provably wrong. – Wayne Burkett Nov 22 '13 at 16:32
                    You cannot validate email addresses, period. The only one who can validate an email address is the provider of the email address. For example, this answer says these email addresses: %2@gmail.com, "%2"@gmail.com, "a..b"@gmail.com, "a_b"@gmail.com, _@gmail.com, 1@gmail.com , 1_example@something.gmail.com are all valid, but Gmail will never allow any of these email addresses. You should do this by accepting the email address and sending an email message to that email address, with a code/link the user must visit to confirm validity. – Kevin Fegan Feb 1 '14 at 8:49 
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

</div>
<!--end of syarat ketentuan--> 
<script lang="javascript">
//    begin of skrip lupa password
    $('#lupa_password_modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Reset password ')
    })

//    end of skrip lupa password

//    begin of skrip syarat dan ketentuan
    $('#syarat_ketentuan').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('SYARAT DAN KETENTUAN ')
    })
//    end of skrip syarat dan ketentuan 

    //fungsi validasi email 
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function validate() {
        var $result = $("#result");
        var email = $("#alamat_email").val();
        $result.text("");

        if (validateEmail(email)) {
            $result.text("Telah kami kirimkan link reset password, harap cek email " + email);
            $result.css("color", "green");
            //do some logic here below
            //...

        } else {
            $result.text(email + " is not valid ");
            $result.css("color", "red");
        }
        return false;
    }

    $("#send").on("click", validate);
</script>